﻿namespace FieldProbe
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.btnProbe = new System.Windows.Forms.Button();
            this.chkProbeAll = new System.Windows.Forms.CheckBox();
            this.radLeftRight = new System.Windows.Forms.RadioButton();
            this.radInner = new System.Windows.Forms.RadioButton();
            this.txtXPath = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSavedFields = new System.Windows.Forms.TextBox();
            this.cboFieldNames = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.radPropValue = new System.Windows.Forms.RadioButton();
            this.radClassContains = new System.Windows.Forms.RadioButton();
            this.radTextEquals = new System.Windows.Forms.RadioButton();
            this.radTextContains = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Page URL:";
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.Location = new System.Drawing.Point(80, 5);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(734, 20);
            this.txtUrl.TabIndex = 1;
            this.txtUrl.Text = "http://localhost:8080/Viec_Lam_Android_Developer.htm";
            this.toolTip1.SetToolTip(this.txtUrl, "URL of the page to probe.");
            this.txtUrl.TextChanged += new System.EventHandler(this.txtUrl_TextChanged);
            this.txtUrl.Leave += new System.EventHandler(this.txtUrl_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(80, 31);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(247, 20);
            this.txtKey.TabIndex = 3;
            this.toolTip1.SetToolTip(this.txtKey, "Property Name, default: \"itemprop\"");
            this.txtKey.TextChanged += new System.EventHandler(this.txtKey_TextChanged);
            // 
            // btnProbe
            // 
            this.btnProbe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProbe.Location = new System.Drawing.Point(820, 2);
            this.btnProbe.Name = "btnProbe";
            this.btnProbe.Size = new System.Drawing.Size(75, 23);
            this.btnProbe.TabIndex = 18;
            this.btnProbe.Text = "&Probe";
            this.btnProbe.UseVisualStyleBackColor = true;
            this.btnProbe.Click += new System.EventHandler(this.btnProbe_Click);
            // 
            // chkProbeAll
            // 
            this.chkProbeAll.AutoSize = true;
            this.chkProbeAll.Location = new System.Drawing.Point(83, 85);
            this.chkProbeAll.Name = "chkProbeAll";
            this.chkProbeAll.Size = new System.Drawing.Size(68, 17);
            this.chkProbeAll.TabIndex = 8;
            this.chkProbeAll.Text = "Probe &All";
            this.chkProbeAll.UseVisualStyleBackColor = true;
            // 
            // radLeftRight
            // 
            this.radLeftRight.AutoSize = true;
            this.radLeftRight.Checked = true;
            this.radLeftRight.Location = new System.Drawing.Point(180, 82);
            this.radLeftRight.Name = "radLeftRight";
            this.radLeftRight.Size = new System.Drawing.Size(71, 17);
            this.radLeftRight.TabIndex = 9;
            this.radLeftRight.TabStop = true;
            this.radLeftRight.Text = "Left-Right";
            this.toolTip1.SetToolTip(this.radLeftRight, "By Name:Value pattern, eg: //*[starts-with(text(), \'Cấp bậc\')]/following-sibling:" +
        ":*[1]/node()");
            this.radLeftRight.UseVisualStyleBackColor = true;
            this.radLeftRight.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // radInner
            // 
            this.radInner.AutoSize = true;
            this.radInner.Location = new System.Drawing.Point(339, 82);
            this.radInner.Name = "radInner";
            this.radInner.Size = new System.Drawing.Size(73, 17);
            this.radInner.TabIndex = 11;
            this.radInner.Text = "Inner Text";
            this.toolTip1.SetToolTip(this.radInner, "By Heading Text, eg: //*[starts-with(text(), \'sss\')]/../node()");
            this.radInner.UseVisualStyleBackColor = true;
            this.radInner.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // txtXPath
            // 
            this.txtXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtXPath.Location = new System.Drawing.Point(80, 56);
            this.txtXPath.Name = "txtXPath";
            this.txtXPath.Size = new System.Drawing.Size(734, 20);
            this.txtXPath.TabIndex = 7;
            this.toolTip1.SetToolTip(this.txtXPath, "XPath expression.");
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(16, 108);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.txtResult);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnExport);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.txtSavedFields);
            this.splitContainer1.Size = new System.Drawing.Size(879, 405);
            this.splitContainer1.SplitterDistance = 201;
            this.splitContainer1.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Results:";
            // 
            // txtResult
            // 
            this.txtResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResult.Location = new System.Drawing.Point(88, 3);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResult.Size = new System.Drawing.Size(710, 195);
            this.txtResult.TabIndex = 1;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(804, 5);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "&Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Saved Fields:";
            // 
            // txtSavedFields
            // 
            this.txtSavedFields.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSavedFields.Location = new System.Drawing.Point(88, 2);
            this.txtSavedFields.Multiline = true;
            this.txtSavedFields.Name = "txtSavedFields";
            this.txtSavedFields.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSavedFields.Size = new System.Drawing.Size(709, 197);
            this.txtSavedFields.TabIndex = 1;
            // 
            // cboFieldNames
            // 
            this.cboFieldNames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboFieldNames.FormattingEnabled = true;
            this.cboFieldNames.Location = new System.Drawing.Point(642, 30);
            this.cboFieldNames.Name = "cboFieldNames";
            this.cboFieldNames.Size = new System.Drawing.Size(171, 21);
            this.cboFieldNames.TabIndex = 17;
            this.toolTip1.SetToolTip(this.cboFieldNames, "Field to save to config file.");
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(820, 29);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(604, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Field:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Value:";
            // 
            // txtValue
            // 
            this.txtValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValue.Location = new System.Drawing.Point(376, 31);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(222, 20);
            this.txtValue.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtValue, "Property value.");
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 100;
            // 
            // radPropValue
            // 
            this.radPropValue.AutoSize = true;
            this.radPropValue.Location = new System.Drawing.Point(418, 82);
            this.radPropValue.Name = "radPropValue";
            this.radPropValue.Size = new System.Drawing.Size(77, 17);
            this.radPropValue.TabIndex = 12;
            this.radPropValue.Text = "Prop Value";
            this.toolTip1.SetToolTip(this.radPropValue, "By Property Value, eg: //div[@itemprop=\'title\']/h1/text()");
            this.radPropValue.UseVisualStyleBackColor = true;
            this.radPropValue.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // radClassContains
            // 
            this.radClassContains.AutoSize = true;
            this.radClassContains.Location = new System.Drawing.Point(501, 82);
            this.radClassContains.Name = "radClassContains";
            this.radClassContains.Size = new System.Drawing.Size(93, 17);
            this.radClassContains.TabIndex = 13;
            this.radClassContains.Text = "Class contains";
            this.toolTip1.SetToolTip(this.radClassContains, "By Class Value, eg: //div[contains(concat(\' \', @class, \' \'), \' job_requirement en" +
        "d \')]//li/text()");
            this.radClassContains.UseVisualStyleBackColor = true;
            this.radClassContains.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // radTextEquals
            // 
            this.radTextEquals.AccessibleDescription = "";
            this.radTextEquals.AutoSize = true;
            this.radTextEquals.Location = new System.Drawing.Point(251, 82);
            this.radTextEquals.Name = "radTextEquals";
            this.radTextEquals.Size = new System.Drawing.Size(76, 17);
            this.radTextEquals.TabIndex = 10;
            this.radTextEquals.Text = "Text Equal";
            this.toolTip1.SetToolTip(this.radTextEquals, "By Text Value, eg: //p[text() = \'Lương\']/../p[2]/text()");
            this.radTextEquals.UseVisualStyleBackColor = true;
            this.radTextEquals.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // radTextContains
            // 
            this.radTextContains.AccessibleDescription = "";
            this.radTextContains.AutoSize = true;
            this.radTextContains.Location = new System.Drawing.Point(600, 82);
            this.radTextContains.Name = "radTextContains";
            this.radTextContains.Size = new System.Drawing.Size(89, 17);
            this.radTextContains.TabIndex = 14;
            this.radTextContains.Text = "Text contains";
            this.toolTip1.SetToolTip(this.radTextContains, "By Text Value, eg: //li[contains(text(), \'Kinh nghiệm:\')]/text()");
            this.radTextContains.UseVisualStyleBackColor = true;
            this.radTextContains.CheckedChanged += new System.EventHandler(this.radXPathType_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "XPath Expr:";
            // 
            // FrmMain
            // 
            this.AcceptButton = this.btnProbe;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 525);
            this.Controls.Add(this.radTextContains);
            this.Controls.Add(this.radTextEquals);
            this.Controls.Add(this.radClassContains);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cboFieldNames);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.txtXPath);
            this.Controls.Add(this.radPropValue);
            this.Controls.Add(this.radInner);
            this.Controls.Add(this.radLeftRight);
            this.Controls.Add(this.chkProbeAll);
            this.Controls.Add(this.btnProbe);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.label1);
            this.Name = "FrmMain";
            this.Text = "Probe Fields";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Button btnProbe;
        private System.Windows.Forms.CheckBox chkProbeAll;
        private System.Windows.Forms.RadioButton radLeftRight;
        private System.Windows.Forms.RadioButton radInner;
        private System.Windows.Forms.TextBox txtXPath;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TextBox txtSavedFields;
        private System.Windows.Forms.ComboBox cboFieldNames;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radPropValue;
        private System.Windows.Forms.RadioButton radClassContains;
        private System.Windows.Forms.RadioButton radTextEquals;
        private System.Windows.Forms.RadioButton radTextContains;
    }
}

