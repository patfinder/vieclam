﻿using HtmlAgilityPack;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;

namespace FieldProbe
{
    public partial class FrmMain : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FrmMain));

        private string RadXPathTypeCaption = null;
        private string PageContent = null;
        private Dictionary<string, string> SavedFields = new Dictionary<string,string>();

        public enum XPathType
        {
            LeftRight = 1,
            InnerText,
            PropValue,
            ClassContains,
            TextEqual,
            TextContains,
        }

        public Dictionary<string, XPathType> GetXPathTypeMap()
        {
            return new Dictionary<string, XPathType>()
            {
                {radLeftRight.Text, XPathType.LeftRight},
                {radInner.Text, XPathType.InnerText},
                {radPropValue.Text, XPathType.PropValue},
                {radClassContains.Text, XPathType.ClassContains},
                {radTextEquals.Text, XPathType.TextEqual},
                {radTextContains.Text, XPathType.TextContains},
            };
        }

        public FrmMain()
        {
            InitializeComponent();

            LoadPage();

            var itemFieldNames = ConfigurationManager.AppSettings["ItemFields"].Split(new char[] { ',' }).Select(s => s.Trim()).ToList();
            cboFieldNames.Items.Clear();

            foreach (string field in itemFieldNames)
            {
                cboFieldNames.Items.Add(field);
            }

            RadXPathTypeCaption = radLeftRight.Text;
        }

        private void LoadPage()
        {
            try
            {
                // Load page
                WebClient webClient = new WebClient();
                webClient.Encoding = Encoding.UTF8;

                PageContent = System.Net.WebUtility.HtmlDecode(webClient.DownloadString(txtUrl.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
                log.Error("LoadPage", ex);
            }
        }

        private static string ExtractContent(HtmlNodeCollection nodes)
        {
            string result = "";

            foreach (HtmlNode node in nodes)
            {
                 string text = node.InnerText;

                result += text + Environment.NewLine;
            }

            result += Environment.NewLine + "******************" + Environment.NewLine;

            foreach (HtmlNode node in nodes)
            {
                string text = node.InnerHtml;

                result += text + Environment.NewLine;
            }

            return result;
        }

        private void btnProbe_Click(object sender, EventArgs e)
        {
            try
            {
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.Load(new StringReader(PageContent));

                txtResult.Text = "";

                if (chkProbeAll.Checked)
                {
                    var xpTypes = new XPathType[]{
                    XPathType.LeftRight,
                    XPathType.InnerText,
                    XPathType.PropValue,
                    XPathType.ClassContains,
                    XPathType.TextEqual,
                    XPathType.TextContains,
                };

                    foreach (var xpType in xpTypes)
                    {
                        var nodes = doc.DocumentNode.SelectNodes(GetXPathExpression(xpType, txtKey.Text, txtValue.Text));
                        txtResult.Text += xpType.ToString() + " =======" + Environment.NewLine;

                        if (nodes != null)
                        {
                            txtResult.Text += ExtractContent(nodes);
                        }
                        txtResult.Text += Environment.NewLine + "========================================================" + Environment.NewLine;
                    }
                }
                else
                {
                    var xpType = GetXPathTypeMap()[RadXPathTypeCaption];
                    var nodes = doc.DocumentNode.SelectNodes(txtXPath.Text);
                    txtResult.Text += xpType.ToString() + " =======" + Environment.NewLine;

                    if (nodes != null)
                    {
                        txtResult.Text += ExtractContent(nodes);
                    }
                    txtResult.Text += Environment.NewLine + "========================================================" + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
                log.Error("btnProbe_Click", ex);
            }
        }

        private void btnProbe_Click__(object sender, EventArgs e)
        {
            if (PageContent == null)
            {
                MessageBox.Show("Please input page URL.");
                return;
            }

            TextReader textReader = new StringReader(PageContent);
            XmlTextReader xmlTextReader = new XmlTextReader(textReader);
            xmlTextReader.XmlResolver = null;

            // Parse page content
            XPathDocument doc = new XPathDocument(xmlTextReader);
            //XPathDocument doc = new XPathDocument(txtUrl.Text);
            XPathNavigator nav = doc.CreateNavigator();

            // Compile a standard XPath expression
            XPathExpression expr = nav.Compile(txtXPath.Text);
            XPathNodeIterator iterator = nav.Select(expr);

            // Iterate on the node set
            txtResult.Text = "";
            try
            {
                while (iterator.MoveNext())
                {
                    XPathNavigator nav2 = iterator.Current.Clone();

                    txtResult.Text += nav2.Value + Environment.NewLine;

                }
            }
            catch (Exception)
            {
            }
        }

        private void txtUrl_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtUrl.Text))
                return;

            LoadPage();
        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            PageContent = null;
        }

        private string GetXPathExpression()
        {
            return GetXPathExpression(GetXPathTypeMap()[RadXPathTypeCaption], txtKey.Text, txtValue.Text);
        }

        /// <summary>
        /// Get XPath expression from key string and type.
        /// </summary>
        /// <returns></returns>
        private static string GetXPathExpression(XPathType xpathType, string prop, string value)
        {
            value = value.Trim();

            if (xpathType == XPathType.TextEqual)
            {
                // Find node then get sibling
                // //*[text() = 'Lương']/following-sibling::*[1]/node()
                return "//*[text() = '" + value + "']/following-sibling::*[1]/node()";
            }
            else if (xpathType == XPathType.LeftRight)
            {
                // Find node then get sibling
                // //*[starts-with(text(), 'aaaa')]/following-sibling::*[1]/node()
                return "//*[starts-with(text(), '" + value + "')]/following-sibling::*[1]/node()";
            }
            else if (xpathType == XPathType.InnerText)
            {
                // Find node, then get parent
                // //*[starts-with(text(), 'sss')]/../node()
                return "//*[starts-with(text(), '" + value + "')]/../node()";
            }
            else if (xpathType == XPathType.PropValue)
            {
                // //div[@itemprop='title']/node()
                return "//*[@" + prop + "='" + value + "']/node()";
            }
            else if (xpathType == XPathType.ClassContains)
            {
                // //div[contains(concat(' ', @class, ' '), ' job_requirement end ')]//li/text()
                return "//*[contains(concat(' ', @class, ' '), ' " + value + " ')]/node()";
            }
            else if (xpathType == XPathType.TextContains)
            {
                // //*[contains(text(), 'Kinh nghiệm:')]/node()
                return "//*[contains(text(), '" + value + "')]/node()";
            }

            return null;
        }

        private void DoChangeXPath()
        {
            // Change XPath Expression accordingly
            txtXPath.Text = GetXPathExpression();
        }

        private void txtKey_TextChanged(object sender, EventArgs e)
        {
            DoChangeXPath();
        }

        private void radXPathType_CheckedChanged(object sender, EventArgs e)
        {
            RadXPathTypeCaption = ((RadioButton)sender).Text;

            DoChangeXPath();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(cboFieldNames.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a field name (combo) for saving");
                return;
            }

            string field = (string)cboFieldNames.SelectedItem;

            if (SavedFields.ContainsKey(field))
            {
                if (MessageBox.Show("Are you sure you want to overwrite current field XPath?", "Confirm Overwritting", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                    return;

                SavedFields[field] = txtXPath.Text;
            }
            else
                SavedFields.Add(field, txtXPath.Text);

            txtSavedFields.Text = "";

            foreach (var kv in SavedFields)
            {
                txtSavedFields.Text += kv.Key + " <<-->> " + kv.Value + Environment.NewLine;
            }
        }

        private void SaveIFileConfig(ref CollectorIFile ifile)
        {
            var spider = ifile.Spider;
            var spiderType = spider.GetType();

            var props = ifile.ItemProperties;
            var propsType = props.GetType();

            // Excluded Spider properties
            var spiderFields = new string[] { "dryrun", "outFileName", "domain", "page", "ilink", "itemNoU", "multiValueFields" };

            foreach (var kv in SavedFields)
            {
                try
                {
                    // Spider
                    if (spiderFields.Contains(kv.Key))
                        spiderType.InvokeMember(kv.Key,
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetField,
                            Type.DefaultBinder, spider, new[] { kv.Value });
                    else
                        // Item Props
                        propsType.InvokeMember(kv.Key,
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetField,
                            Type.DefaultBinder, props, new[] { kv.Value });
                }
                catch (Exception ex)
                {
                    log.Error("SaveIFileConfig", ex);
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (SavedFields.Count <= 0)
            {
                MessageBox.Show(
                    "Click 'Export' to save collected fields XPath expression into scrapy Collector config file." + Environment.NewLine + 
                    "Please fill 'Saved Fields' list and try again later.");

                return;
            }

            try
            {
                Uri uri = new Uri(txtUrl.Text);
                var host = uri.DnsSafeHost;

                var folder = Application.StartupPath;
                string filePath = Path.Combine(folder, string.Format("ifile-{0}.json", host));

                CollectorIFile ifile = new CollectorIFile();

                // Set page
                if (SavedFields.ContainsKey("page"))
                    SavedFields["page"] = txtUrl.Text;
                else
                    SavedFields.Add("page", txtUrl.Text);

                // Set domain
                if (SavedFields.ContainsKey("domain"))
                    SavedFields["domain"] = host;
                else
                    SavedFields.Add("domain", host);

                // Save field config to ifile
                SaveIFileConfig(ref ifile);

                var ifileStr = JsonConvert.SerializeObject(ifile, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(filePath, ifileStr);

                MessageBox.Show("IFile config file saved to: " + filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
                log.Error("btnExport_Click", ex);
            }
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            DoChangeXPath();
        }
    }

    class CollectorIFile
    {
        public class Spider_
        {
            public bool dryrun = false;
            public string outFileName = "ie = cntt-phan-mem";
            public string domain = "ie = careerbuilder.vn";
            public string page = "ie = http =//careerbuilder.vn/vi/tim-viec-lam/nganh-nghe/cntt-phan-mem/limit/20/page/##page##";
            public string ilink = "ie = //span[@class='rc_col_317px jobtitle']/a/@href";
            public string itemNoU = "ie = .*\\.(\\w+)\\.html";
            public string[] multiValueFields = new string[] { "jobIndustries", "province" };
        }
        public Spider_ Spider = new Spider_();

        public class ItemProperties_
        {
            public string title = null;
            public string description = null;
            public string itemNo = null;
            
            public string jobIndustries = null;
            public string jobSubClasses = null;
            public string jobType = null;
            public string jobTypeString = null;
            public string companyID = null;
            public string companyName = null;
            public string provinceID = null;
            public string province = null;
            public string district = null;
            public string addressID = null;
            public string addressString = null;
            public string salaryType = null;
            public string salaryTypeString = null;
            public string salaryMin = null;
            public string salaryMax = null;
            public string workHours = null;
            public string status = null;
            public string postTime = null;
            public string expiredTime = null;
            public string rating = null;
            public string workingYears = null;
            public string workRequirements = null;
            public string workTitle = null;
            public string workLevel = null;
            public string contact = null;
        }
        public ItemProperties_ ItemProperties = new ItemProperties_();
    }
}
