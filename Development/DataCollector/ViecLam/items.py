# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PostItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    # Special fields - should exist in all object type
    itemNo = scrapy.Field()
    itemLink = scrapy.Field()

    # Specific fields
    jobIndustries = scrapy.Field()
    jobSubClasses = scrapy.Field()
    jobType = scrapy.Field()
    jobTypeString = scrapy.Field()
    companyID = scrapy.Field()
    companyName = scrapy.Field()
    provinceID = scrapy.Field()
    province = scrapy.Field()
    district = scrapy.Field()
    addressID = scrapy.Field()
    addressString = scrapy.Field()
    salaryType = scrapy.Field()
    salaryTypeString = scrapy.Field()
    salaryMin = scrapy.Field()
    salaryMax = scrapy.Field()
    workHours = scrapy.Field()
    status = scrapy.Field()
    postTime = scrapy.Field()
    expiredTime = scrapy.Field()
    rating = scrapy.Field()
    workingYears = scrapy.Field()
    workRequirements = scrapy.Field()
    workTitle = scrapy.Field()
    workLevel = scrapy.Field()
    contact = scrapy.Field()
