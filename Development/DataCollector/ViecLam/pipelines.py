# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
# import os.path
import json
import codecs
import unicodedata
from datetime import datetime

from collections import OrderedDict
from scrapy import signals
from scrapy.contrib.exporter import *
from xml.sax.saxutils import escape
from ViecLam.items import *
from ViecLam.spiders.sitespider import SiteSpider


class SeekBaseExporterPipeline(object):
    def __init__(self):
        self.files = {0: "Testing"}
        self.format = ""
        self.exporter = None

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        # Get current local time
        ts = datetime.today().strftime("%Y-%m-%d")
        file = open('%s_%s.%s' % (SiteSpider.outFileName, ts, self.format), 'w+b')
        self.files[spider.name] = file
        self.exporter = JsonItemExporter(file) if self.format == "json" \
            else XmlItemExporter(file) if self.format == "xml" \
            else CsvItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider.name)
        file.close()

    def process_item(self, item, spider):
        """
        :param item: Extracted item
        :type item: dict
        :param spider:
        :type spider:
        :return:
        :rtype:
        """

        pass


class JsonExportPipeline(SeekBaseExporterPipeline):

    def __init__(self):
        super(SeekBaseExporterPipeline, self).__init__()
        self.format = "json"

    def process_item(self, item, spider):
        """
        :param item: Extracted item
        :type item: dict
        :param spider:
        :type spider:
        :return:
        :rtype:
        """

        item2 = PostItem()
        # {k: escape(v) for k, v in item.items()}
        for k, v in item.items():
            item2[k] = unicodedata.normalize("NFKD", unicode(v)).encode('ascii', 'ignore').replace("\"", "\\")

        self.exporter.export_item(item2)
        return item


class XmlExportPipeline(SeekBaseExporterPipeline):

    def __init__(self):
        super(SeekBaseExporterPipeline, self).__init__()
        self.format = "xml"

    def process_item(self, item, spider):
        """
        :param item: Extracted item
        :type item: dict
        :param spider:
        :type spider:
        :return:
        :rtype:
        """

        item2 = PostItem()
        # {k: escape(v) for k, v in item.items()}
        for k, v in item.items():
            item2[k] = escape(unicodedata.normalize("NFKD", unicode(v)).encode('ascii', 'ignore'))

        self.exporter.export_item(item2)
        return item


class CsvExportPipeline(SeekBaseExporterPipeline):

    def __init__(self):
        # super(SeekBaseExporterPipeline, self).__init__()
        SeekBaseExporterPipeline.__init__(self)
        self.format = "csv"

    def process_item(self, item, spider):
        """
        :param item: Extracted item
        :type item: dict
        :param spider:
        :type spider:
        :return:
        :rtype:
        """

        # item2 = PostItem()

        # item2['Title'] = unicodedata.normalize("NFKD", unicode(item['title'])).encode('ascii', 'ignore')
        # item2['Job_Link'] = unicodedata.normalize("NFKD", unicode(item['jobLink'])).encode('ascii', 'ignore')
        # item2['Company'] = unicodedata.normalize("NFKD", unicode(item['companyName'])).encode('ascii', 'ignore')
        # item2['Short_Description'] = unicodedata.normalize("NFKD", unicode(item['blurb'])).encode('ascii', 'ignore')
        # item2['Salary'] = unicodedata.normalize("NFKD", unicode(item['salary'])).encode('ascii', 'ignore')
        # item2['City'] = unicodedata.normalize("NFKD", unicode(item['location'])).encode('ascii', 'ignore')
        # item2['Work_Type'] = unicodedata.normalize("NFKD", unicode(item['workType'])).encode('ascii', 'ignore')

        self.exporter.export_item(item)
        return item