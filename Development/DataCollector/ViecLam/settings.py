# -*- coding: utf-8 -*-

# Scrapy settings for ViecLam project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'SiteSpider'

SPIDER_MODULES = ['ViecLam.spiders']
NEWSPIDER_MODULE = 'ViecLam.spiders'

ITEM_PIPELINES = {
    # 'SeekJobs.pipelines.JsonPipeline': 300,
    # 'SeekJobs.pipelines.JsonExportPipeline': 300,
    # 'SeekJobs.pipelines.XmlExportPipeline': 301,
    'ViecLam.pipelines.CsvExportPipeline': 301,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ViecLam (+http://www.yourdomain.com)'
