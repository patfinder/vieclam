__author__ = 'admin'
import re

class SpiderUtils(object):

    def __init__(self):

        super(object, self).__init__()
        pass

    @staticmethod
    def read_keys_input(file1):
        """
        Read xpath config from text file and return a tuple of page Url and XPath keys, Text keys
        :param file1: file
        :return: tuple(str, dict{str, str}, dict{str, str})
        """

        lines = [l.strip(' \t\n\r') for l in file1.readlines()]

        # Skip comment lines and empty lines
        lines = [l for l in lines if not l.startswith("#") and l]

        # Get first URL line
        page = lines[0]

        propKeys = [[m.strip() for m in l.split(u"=", 1)] for l in lines[1:]]

        # xpath lines currently look likes
        # 1. // ...
        # 2. func(...)

        p = re.compile(r"\w+\(.*\)$")

        xpath_lines = []
        text_lines = []

        # xpath lines
        for k, v in propKeys:
            if v.startswith("//") or p.match(v):
                xpath_lines += [(k, v)]
            else:
                text_lines += [(k, v)]

        return page, xpath_lines, text_lines

