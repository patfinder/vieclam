# -*- coding: utf-8 -*-
import scrapy
import datetime
import json
import codecs
import re

from ViecLam.spiderutils import *

class ProbeSpider(scrapy.Spider):

    name = "probe"
    allowed_domains = []
    # start_urls = (
    #     'http://www.domain.com/',
    # )

    def __init__(self, *args, **kwargs):
        super(ProbeSpider, self).__init__(*args, **kwargs)

        # Store ifile name
        self.gen_ifile = False if "gen-ifile" not in kwargs else kwargs["gen-ifile"]

        file1 = codecs.open("probe.txt", 'r', encoding='utf-8')
        self.page, self.propKeys, self.propTexts = SpiderUtils.read_keys_input(file1)
        file1.close()

    def start_requests(self):

        # Load info from probe.json
        return [scrapy.Request(self.page)]

    def parse(self, response):

        results = []

        # XPath result
        for propName, propKey in self.propKeys:
            item = (propName, propKey)
            item += (response.xpath(propKey).extract(),)

            results += [item]

        # Text result
        for propName, propKey in self.propTexts:
            item = (propName, propKey)
            key = "//*[starts-with(text(), '%s')]/../node()" % propKey
            value = response.xpath(key).extract()
            value = [v for v in value and v.strip()]
            item += (value,)

            results += [item]

        # Save result to file
        ts = datetime.datetime.today().strftime("%Y-%m-%d")
        file1 = codecs.open('probe_out_%s.json' % ts, 'w+', encoding='utf-8')
        # file1 = codecs.open('probe_out_%s.json' % ts, 'w+b')

        json.dump(results, file1, indent=4)
        file1.close()
