# -*- coding: utf-8 -*-
from __builtin__ import staticmethod
import scrapy
import urllib
from scrapy import log
from scrapy.http.request import Request
from scrapy.http.response import Response
from scrapy.http.response.text import TextResponse
import re
import ConfigParser
import codecs
import json
import datetime

from ViecLam.items import *

class SiteSpider(scrapy.Spider):

    outFileName = None
    MinEmptyPage = 999
    MaxPages = 50

    itemCount = 0
    name = "SiteSpider"
    excludePropList = ["itemNoU", ]
    allowed_domains = []

    # start_urls = (
    # 'http://www.thuephongtro.com/',
    # )

    def __init__(self, *args, **kwargs):
        super(SiteSpider, self).__init__(*args, **kwargs)

        self.propKeys = {}
        """Dict for property keys"""
        self.params = {}

        # Get item key from file (ifile)
        ifile = None if "ifile" not in kwargs else kwargs["ifile"]

        if not ifile:
            raise Exception("ifile argument must be provided. See 'Readme' file for more info.")

        # Read params from config file
        with codecs.open(ifile, "r", "utf8") as configFile:
            configData = json.load(configFile)

            self.params = configData["Spider"]
            # All property keys are lists
            self.propKeys = dict([(k, v) if isinstance(v, list) else (k, [v]) for k, v in configData["ItemProperties"].items()])
            # self.propKeys = configData["ItemProperties"]

        # itemNoU
        self.itemNoU = None if "itemNoU" not in self.params else self.params["itemNoU"]

        # page, domain, dryrun
        page = self.params["page"]

        SiteSpider.outFileName = self.params["outFileName"]
        self.domain = self.params["domain"]
        self.multiValueFields = self.params["multiValueFields"]
        self.host = ("http://" if page.startswith("http://") else "https://") + self.domain  # + "/"
        self.dryrun = SiteSpider.strtobool(None if "dryrun" not in self.params else self.params["dryrun"])

    def start_requests(self):

        # Build initial request URL
        url = self.params["page"].replace("##page##", "1")

        return [scrapy.Request(url)]

    def parse(self, response):
        """
        :param response: Reponse content
        :type response: TextResponse
        :return:
        """

        try:
            # Check if this is a listing page.
            # TODO: Check URL and RegEx conflict (escape RegEx special chars)
            #url = re.escape(self.params["page"]).replace("##page##", "(\\d+)")
            url = re.escape(self.params["page"].replace("##page##", "THEPAGETHE"))
            url = url.replace("THEPAGETHE", "(\\d+)")
            pattern = re.compile(url)
            match = pattern.match(response.url)

            newPageList = []

            # =================================================
            # This is a listing page
            # Second or match page URL without a page number pattern
            if match:

                # =================================================
                # groups > 0 // There is a ##page## pattern in page URL.
                # Create page list if this page is 1 or dividable to 10
                page = 0
                if pattern.groups > 0:
                    page = int(match.group(1))

                    # MaxPages: Prevent endless loop
                    if page == 1 or page % 10 == 0 and page < SiteSpider.MaxPages \
                            and page < SiteSpider.MinEmptyPage:  # Page 11 duplicate is ok
                        # Add next 10 pages
                        newPageList = [Request(self.params["page"].replace("##page##", str(p)))
                                       for p in range(page + 1, page + 11)]

                # =================================================
                # Get item urls of this page; ilink
                linkKey = self.params["ilink"]

                # Check if URL is relative or not including domain, then normalize
                itemPageUrls = [Request(("" if u.startswith("http") else self.host) + u) for u in response.xpath(linkKey).extract()]

                if not itemPageUrls:
                    SiteSpider.MinEmptyPage = SiteSpider.MinEmptyPage if not page or page > SiteSpider.MinEmptyPage else page

                return newPageList + itemPageUrls

            # =================================================
            # This is a detail page

            if self.dryrun and SiteSpider.itemCount >= 30:
                return None

            SiteSpider.itemCount += 1

            # Create post item
            post = PostItem()
            post['itemLink'] = response.url
            # Parse item No from URL
            if self.itemNoU:
                p = re.compile(self.itemNoU)
                m = p.match(response.url)
                post["itemNo"] = None if not m else m.group(1)

            # =================================================
            # This is a detail page
            for propName, propKey in self.propKeys.iteritems():

                # Skip empty key also
                if not propKey:
                    continue

                elemValue = []
                for k in [k for k in propKey if k]:

                    value = response.xpath(k).extract()
                    if value:
                        if propName in self.multiValueFields:
                            elemValue += ([" <<->> "] if elemValue and any(elemValue) else []) + value
                        else:
                            # Stop searching
                            elemValue = value
                            break

                if not elemValue or not any(elemValue):
                    continue

                post[propName] = SiteSpider.strip_str(" ".join(elemValue))

                # codeKey = None if "item_code" not in self.params else self.params["item_code"]
                # post['code'] = SiteSpider.strip_str((response.xpath(codeKey).extract() or [None])[0])

            return post
            # pass

        except Exception as ex:
           self.log(str(ex), log.DEBUG)
           raise ex

    @staticmethod
    def strip_str(str1):
        """
        Return striped input string if it is a string, other None
        :param str1: str
        :return: str or None
        """

        if isinstance(str1, str):
            return str1.strip()

        if isinstance(str1, unicode):
            return str1.strip()

        return None

    @staticmethod
    def strtobool(str1):
        """
        Return bool value from string: True = 1, true, yes ...
        :param str1: str
        :return:
        """
        return (isinstance(str1, str) or isinstance(str1, unicode)) and \
               str1.strip().lower() in ['true', '1', 't', 'y', 'yes', 'on', 'yeah', 'yup', 'certainly', 'uh-huh']

