# TODO: Change params to human-readable format
# TODO: Which types of params to support?
# TODO: Check notes in sections below
# TODO: WorkRequirements => Bằng Cấp (Đại học); Chứng chỉ; Độ tuổi ?
    Extra (custom) fields?

# Required Packages
    service_identity, win32api, python-dateutil

# ============================================================================
# Scrapy - Collect products (posts) item from websites
# Currently the program focus on House and Room Sales and Rents
#
# How It Works:
#
#  0. Running Modes (depreciated)
#
#  1. The spider will browse through an initial link, collect listing links from that page and paged sub-page
#
#  2. Spider then crawls through listing page, get detail page URLs
#
#  3. Spider crawls details page to get product details
#
#  4. The listing page url will be provided in command line. The page number will start at 1 and follow following format
#       param: page
#       eg:    page="http://thuephongtro.com/page-##page##.html"
#       match: http://thuephongtro.com/page-2.html
#       where ##page## will be replaced with the page number and start at 1.
#
#  5. In listing page, the item detail page URLs will be identified by an xpath string provided in command line
#       param: ilink
#       eg: ilink="//div[@id='item_link']"
#
#  6. In detail page, item details xpath key will be provided from command line as following
#       eg: item_code="//div[@id='item_id']"
#           item_title      # post title
#           item_desc       # Detail description
#           item_time       # post time
#           item_ownerid    # Owner ID
#           item_ownername  # Owner name
#           item_type       # house/room/room-mate
#           item_price      # Price
#           item_area       # Diện tích
#           item_address
#           item_city
#           item_district
#           item_image      # House image
#           item_status     # Active / Pending / Expired
#           item_expire     # Post expire time
#           item_views      # View count
#
#  8. Load from file:
#       param: ifile
#       Item property xpath keys can be loaded from file. Each property is on one line without string escape
#       Property key name is the same as above
#
#  7. Extra params:
#       param: domain  # current site domain
#       param: dryrun  # 1/true: Run trial mode. Collect on few items and lists.

# ============================================================================
# Site URLs:
http://thuephongtro.com/page-2.html
http://batdongsan.com.vn/cho-thue-nha-tro-phong-tro/p2
http://www.chotot.vn/tp-ho-chi-minh/cho-thue-phong-cho-thue?o=2
http://rongbay.com/TP-HCM/Thue-va-cho-thue-nha-c272-t634-trang2.html
http://enbac.com/Ho-Chi-Minh/c571/Cho-thue-nha-o/page-2
http://muaban.net/cho-thue-nha-dat-ha-noi-l24-c34/2


# ============================================================================
# Sample commands:

# -s LOG_FILE=scrapy.log

# Note: PyCharm debug param not include "scrapy"
scrapy crawl seek
        -a domain="careerbuilder.vn"
        -a page="http://careerbuilder.vn/vi/tim-viec-lam/nganh-nghe/an-ninh-bao-ve?pppp=##page##"
        -a ilink="//span[@class='rc_col_317px jobtitle']/a/@href"
        -s LOG_FILE=careerbuilder_vn.log

# ============================================================================

scrapy crawl seek
        -a domain="thuephongtro.com"
        -a page="http://thuephongtro.com/page-##page##.html"
        -a ilink="//div[@class = 'd2']//b/a/@href"
        -a item_code="//div[@class = 'bd mf10']/div[@class = 'ptdr']/div[1]/i/text()"
        -a item_code="//div[@class = 'bd mf10']/div[@class = 'ptdr']/div[1]/i/text()"
        -s LOG_FILE=thuephongtro_com.log

        
# ============================================================================
scrapy crawl SiteSpider
        -a ifile=ifile.txt
        -a domain=careerbuilder.vn
        -a page="http://careerbuilder.vn/vi/tim-viec-lam/nganh-nghe/an-ninh-bao-ve?pppp=##page##"
        -a ilink="//span[@class='rc_col_317px jobtitle']/a/@href"
        -s LOG_FILE=vieclam.log


# ============================================================================
# TODO: edit
# Send Mail: send_email.php is the script used for sending search result email
# Notes: N/A


# Required libs:
    PHPMailer: https://github.com/PHPMailer/PHPMailer

# ============================================================================
# Field Key Types

title: //*[@itemprop='title']/h1/text()
workingYears: //*[contains(concat(' ', @class, ' '), ' job_requirement end ')]//li/text()
expiredTime: //*[starts-with(text(), 'Hết hạn nộp')]/following-sibling::*[1]/node()
salaryMin: //*[text() = 'Lương']/../p[2]/text()
workingYears: //*[contains(text(), 'Kinh nghiệm:')]/text()

postTime: substring(//*[@class='datepost']/text(), 12)

# ============================================================================
