CREATE DATABASE  IF NOT EXISTS `vieclam` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `vieclam`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: vieclam
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(45) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `JobNo` varchar(45) DEFAULT NULL,
  `JobLink` varchar(45) DEFAULT NULL,
  `JobType` varchar(45) DEFAULT NULL COMMENT 'Chuyên môn; Phổ thông; Bán thời gian; Thời vụ; ',
  `JobTypeString` varchar(45) DEFAULT NULL,
  `CompanyID` int(11) DEFAULT NULL,
  `CompanyName` varchar(45) DEFAULT NULL,
  `ProvinceID` int(11) DEFAULT NULL,
  `Province` varchar(45) DEFAULT NULL,
  `District` varchar(45) DEFAULT NULL,
  `AddressID` int(11) DEFAULT NULL,
  `AddressString` varchar(45) DEFAULT NULL,
  `SalaryType` int(11) DEFAULT NULL COMMENT 'Need explanation.',
  `SalaryTypeString` varchar(45) DEFAULT NULL,
  `SalaryMin` float DEFAULT NULL,
  `SalaryMax` float DEFAULT NULL,
  `WorkHours` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `PostTime` datetime DEFAULT NULL,
  `ExpiredTime` datetime DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `WorkingYears` int(11) DEFAULT NULL COMMENT 'Số năm kinh nghiệm',
  `WorkRequirements` varchar(45) DEFAULT NULL COMMENT '- Có bằng Đại học / Cao đẳng;\n- Làm ca / Ngoài giờ\n- Bằng cấp: Đại học\n- Độ tuổi: 28 - 40\n- Giới tính: Nam\n- Ngoại hình: ...',
  `WorkTitle` varchar(45) DEFAULT NULL,
  `WorkLevel` varchar(45) DEFAULT NULL COMMENT 'Giám đốc; Quản lý; Trưởng phòng; Trưởng nhóm ...',
  `Contact` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'vieclam'
--

--
-- Dumping routines for database 'vieclam'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-07  0:18:24
